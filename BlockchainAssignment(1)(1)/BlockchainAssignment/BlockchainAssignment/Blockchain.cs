﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BlockchainAssignment
{
    // this is the class where our blockchain actually is created
    //block.cs is the class where we define our blocks and how they get created
    class Blockchain
    {
        public List<Block> Blocks = new List<Block>();// this is a list that will hold all our blocks from block.cs
        

        public List<Transaction> transactionPool; // list to hold all transactions in blockchain 
        private int transactionsPerBlock = 5;





        public Blockchain() // constructor for this class
        {
            Blocks.Add(new Block());// we create the genusis block in here that we have defined in block.cs

            transactionPool = new List<Transaction>(); 

        }

        // now lets print out genesis block to the UI 

        public String GetBlockAsString(int index)
        {
            return Blocks[index].ToString();// return blockindex and tostring() method
            // the twostring method is defined in block.cs that gives all the other varibales of the 
            // blocks eg time stamp and hash values etc 
            // so essentially we call the block index and then use tostring to get the result of the data
            // for Blocks which is the genuisis block

        }

        public Block GetLastBlock() // function to get the last block in the blockchain
        {
            return Blocks[Blocks.Count - 1]; // gets the last block in the bchain
        }




        // Retrieve pending transactions and remove from pool ie the ones left to add to chain
        public List<Transaction> GetPendingTransactions()
        {
            // Determine the number of transactions to retrieve dependent on the number of pending transactions and the limit specified
            int n = Math.Min(transactionsPerBlock, transactionPool.Count);
            // above tells us number of transactions to retrieve out of the pool of transactions 
            // we limited this number to 5 

            // "Pull" transactions from the transaction list (modifying the original list)
            List<Transaction> transactions = transactionPool.GetRange(0, n);
            transactionPool.RemoveRange(0, n);

            // Return the extracted transactions
            return transactions;
        }









        public override string ToString()
        {
            String output = string.Empty;
            Blocks.ForEach(b => output += (b.ToString() + "\n"));

            return output;
        }

        public double GetBalance(String address) // function to get miners balance 
        { // methods interates through all blocks and checks hashes to see if they match to prev 
            // we want to know if the balance is sufficient and the funds are available 
            double balance = 0.0; // intialise it to 0
            foreach(Block b in Blocks)
            {
                foreach(Transaction t in b.transactionList)
                {
                    if (t.recipientAddress.Equals(address)) // if recipients address = address ie if the recipient ie this node created this transaction
                    {
                        // so essentially we are backtracking transactions to see how much balance they had
                        balance += t.amount; // then balance is = to the balance they have plus the amount they sent for that transaction

                    }

                    if (t.senderAddress.Equals(address)) // if the senders address matches the address
                    {
                        balance -= (t.amount +t.fee); // then minus this amount from this balance

// so essentially if there the reciever then add this to there balance if there the sender they minus it from there balance

                    }

                }
            }

            return balance;
        }

        public static bool ValidateHash(Block b)
        {
            String rehash = b.CreateHash();
            return rehash.Equals(b.hash);
        }



        public bool validateMerkleRoot(Block b) // as we create merkle root in block.cs we need to validate our merkle root 
        {
            String reMerkle = Block.MerkleRoot(b.transactionList);
            return reMerkle.Equals(b.merkleRoot);

        }






        // functions for the difficulty task 




        // this is the function adjusts the difficulty
        // this function does not adjust it after every block
        // isntead it mines a few blocks then calculates their average of those blocks 
        // if that average of last few blocks is bigger than threshold or below then adjust difficulty.

        public static int Target_time = 10; // this defines the threshold for the block to change diffuculty 
        // essentially 10 seconds is the threshold if its above 10 the difficulty increases else it lowers
        private List<long> blockTimes = new List<long>(); // list to store all block times we need


       
        private const int MaxDiff = 10;


        // function to mine blocks with adaptive difficulty
        // this adjusts the mining difficulty for next block based on elapsed time 
        // which is the time taken for the last block to mine
        
        // method to mine blocks with adaptive difficulty 
        public void AdaptiveDifficultyMine(Block block, List<Block> blockchain)
        {
            // Initialize a stopwatch to measure elapsed time
            Stopwatch timer = new Stopwatch();

            do
            {
                // Restart stopwatch to measure elapsed time for this block
                timer.Restart();

                // Adding a small delay to ensure accurate timing measurement
                Thread.Sleep(50); // Adjust delay time as needed

                // Perform Proof-of-Work with current difficulty calling mine function 
                block.Mine();

                // Stop the watch to capture the elapsed time
                timer.Stop();

                // Store time in variable and put in milliseconds
                long block_time = timer.ElapsedMilliseconds;
                

                // Add the execution time of this block to the list
                blockTimes.Add(block_time); // this list stores the averager of the last then

                // If there are more than ten blocks in the list, remove the oldest block time
                if (blockTimes.Count > 10)
                    blockTimes.RemoveAt(0);

                // Calculate the average execution time of the last ten blocks
                double averageTime = blockTimes.Average();

                // Adjust the difficulty based on the comparison between average execution time and target block time
                if (averageTime < Target_time * 1000)
                {
                    if (block.difficulty < MaxDiff)
                    {
                        block.difficulty++; // Increase the difficulty
                        Console.WriteLine($"Block Mined in Time: {block_time} ms | Difficulty changed to: {block.difficulty} | because Avg. time of last 10 blocks is: {averageTime} ms");
                    }
                }
                else if (averageTime > Target_time * 1000)
                {
                    if (block.difficulty > 1)
                    {
                        block.difficulty--; // Decrease the difficulty
                        Console.WriteLine($"Block Mined in Time: {block_time} ms | Difficulty changed to: {block.difficulty} | because Avg. time of last 10 blocks is: {averageTime} ms");
                    }
                }

                // Continue loop if the difficulty is not reached max
            } while (block.difficulty < MaxDiff);
        }


       










    }
}
