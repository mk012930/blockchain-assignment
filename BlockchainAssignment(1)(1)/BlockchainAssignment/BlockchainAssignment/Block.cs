﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.NetworkInformation;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BlockchainAssignment
{
     class Block // this will hold the variables of our blocks, this file essentially defines each block 
    {
        public int index; // index of the block in the blockchain ie its position in the chain 
        DateTime timestamp; // the time it was created 
        public string hash; // has function of the current blocks transaction
        public string prevHash; // hash function of the previous block for linkage.

        public List<Transaction> transactionList = new List<Transaction>();
        public int threadCount = 1;

        // proof of work variables
        public long nonce = 0; // we create POW so we need a nonce which starts with 0 it will be incremented
        public int difficulty = 4; //this is the difficulty target that we do to meet the hash 
        // ie we want to create a hash that has a leading start of 4 zeros


        // rewards for winning the POW challenge and fees 
        public double reward = 1.0;
        public double fees = 0.0;

        public String minerAddress = string.Empty; // the address of the node that wins the pow challenge


        public String merkleRoot;






        /* Create the genesis(start) of chain block)*/
        // below we have defined how the blocks are created and hashed and its structure 
        // now we are going to apply this to create the genesis block 

        public Block() // constructor for genesis blocks only
        {
            this.timestamp = DateTime.Now; 
            this.index = 0; // because it is block 0
            this.prevHash = String.Empty; // no previous hash as its the geneusis block
            this.hash = Mine();
            reward = 0; // genesis blocks will all have reward of 0 as its not a competing block 
            // it is just the start block in the bchain 
            
            this.transactionList = new List<Transaction>(); 


        }

        public Block(int index, String hash) // our constructor to intialise our variables
        {
            this.timestamp = DateTime.Now;
            this.index = index + 1; // adding one to index as the blocks increase
            this.prevHash = hash;
            this.hash = Mine(); // this gives us a way to create blocks now ie the hashes and store the results




        }

        public Block(Block lastBlock, List<Transaction> transactions, int Mineversion, String address = "")
        {
            this.timestamp = DateTime.Now;
            this.index = lastBlock.index + 1;
            this.prevHash = lastBlock.hash;
            //this.hash = Mine(); // assigning hash value to the create hash result.
           

            transactions.Add(CreateRewardTransaction(transactions, address));
            this.transactionList = transactions;
            minerAddress= address;
            merkleRoot = MerkleRoot(transactionList);
            if (Mineversion == 1) // 1 means multithreading
                // mine state means 0 if your single threading or 1 if your multithreading.
            {
                hash = ThreadingMine();
            }
            else // if 0 do normal mine
            {
                hash = Mine();

            }


          


        }


        public Transaction CreateRewardTransaction(List<Transaction> transactions, string minerAddress)
        {
            // sum the fees in list of transactions in the mined block
            fees = transactions.Aggregate(0.0, (acc, t) => acc + t.fee);
            // create the transaction reward being sum of fees and reward being transferred to miner rewards coin base
            return new Transaction("Mine Rewards", minerAddress, (reward + fees), 0, "");
        }



        // the function below created is because to create the genusis block we need to actually
        // get the attributes such as index time stamp and hash them
        // it wont have a prevhash value as it is the genusis (starting block)
        public String CreateHash() // function that will create our hash of our block 
        {
            String hash = String.Empty;
            // below we create the hash
            SHA256 hasher = SHA256Managed.Create(); // sha256 is the secure hashing algorithm we use to hash our blocks 
            // this string concatenates all block entities together for hashing
            String input = index.ToString() + timestamp.ToString() + prevHash + nonce.ToString() + reward.ToString() + merkleRoot; // remember a hash is a hash of the entire header added up hence why we add them all together
            Byte[] hashByte = hasher.ComputeHash(Encoding.UTF8.GetBytes(input)); // we are hashing the result of "input"
           
            // we noe will convert our hash results which are in byte representation to string
            foreach (byte x in hashByte) // hashing each byte and produces the string
            {
                hash += String.Format("{0:x2}", x);
            }
            return hash; // returning the resulting hash
        }


        public String Mine() // will define the mining process ie POW that miners will carry out as consensus

        {
            var sw = Stopwatch.StartNew();
            String hash = CreateHash(); // call the create hash function 
            // defining the difficulty criteria and how it should be met
            String re = new string('0', difficulty); // regex to say hash start with 4 0s so we pre-define it
            
            // this for loop will keep executing until the hash is met 
            while (!hash.StartsWith(re)) // if hash doesnt start with 0000
            {
                nonce++; // then increment the nonce value by 1 
                hash = CreateHash(); // create the hash again and re-hash 
            }


            

            return hash; // return the hash

        }



        // function for creating multi threading 
        // Multithreading
        public String ThreadingMine()
        {
            // Initialize variables for multithreading   
            string hasho = null; // store the resulting hash that the thread found
            object lock_result = new object(); //locks the thread result ensuring only one thread can update hash
            int threadcount = 2; // only going to use 2 threads
            ManualResetEvent done = new ManualResetEvent(false); // signals when threads are complete
            bool terminate_threads = false; // way to termainte the threads, flag to know when to terminate

            // Create and start threads
            List<Thread> totalthreads = new List<Thread>(); // 
            for (int i = 0; i < threadcount; i++) // iterate through threads based on thread count
            {
                Thread thread = new Thread(() => // new thread created using lambda function
                {
                    string innerHash = ""; // variable to support the particular threads hash 
                    while (!innerHash.StartsWith(new string('0', difficulty)) && !terminate_threads)
                    // this while loop checks if the generated hash does not start with the required zeros
                    // then dont stop the thread ie the stopthreads flag and keep going until its met 
                    {
                        this.nonce++; // then increment the nonce while this is the case in the while loop
                        innerHash = CreateHash(); // and generate the hashes 
                    }

                    // each thread running independantly increments the nonce value 
                    // the increment nonce is inside of the thread loop ensures each nonce has own unique thread 


                    lock (lock_result)// ensures no duplcation of work 
                    {
                        if (hasho == null) // checks if any other hash has found the right hash yet
                        {
                            hasho = innerHash; // theen assign the hash to our threads hash value meaning its found
                            done.Set(); // tells the other threads to stop
                        }
                    }
                });
                thread.Start(); // start thread to begin executing code defined by lamba function
                totalthreads.Add(thread); // add thread to thread list
                //thread.Join();
            }
            // stop is set to true to indicate threads have finished.
            terminate_threads = true;
            // Wait for all threads to finish
            done.WaitOne();
            return hasho;
        }













        public static String MerkleRoot(List<Transaction> transactionList)
        {
            List<String> hashes = transactionList.Select(t => t.hash).ToList(); // Get a list of transaction hashes for "combining"

            // Handle Blocks with...
            if (hashes.Count == 0) // No transactions
            {
                return String.Empty;
            }
            if (hashes.Count == 1) // One transaction - hash with "self"
            {
                return HashCode.HashTools.CombineHash(hashes[0], hashes[0]);
            }
            while (hashes.Count != 1) // Multiple transactions - Repeat until tree has been traversed
            {
                List<String> merkleLeaves = new List<String>(); // Keep track of current "level" of the tree

                for (int i = 0; i < hashes.Count; i += 2) // Step over neighbouring pair combining each
                {
                    if (i == hashes.Count - 1)
                    {
                        merkleLeaves.Add(HashCode.HashTools.CombineHash(hashes[i], hashes[i])); // Handle an odd number of leaves
                    }
                    else
                    {
                        merkleLeaves.Add(HashCode.HashTools.CombineHash(hashes[i], hashes[i + 1])); // Hash neighbours leaves
                    }
                }
                hashes = merkleLeaves; // Update the working "layer"
            }
            return hashes[0]; // Return the root node
        }


        public override string ToString()
        {
            return "Index: " + index.ToString()
                + "\nTimestamp: " + timestamp.ToString()
                + "\nPrevious Hash:" + prevHash 
                + "\nHash: " + hash +
                "\nNonce: " + nonce.ToString() + // adding to our list of paramaters for our blocks
                "\nDifficulty: " + difficulty.ToString() +
                "\nReward: " + reward.ToString() +
                "\nFees: " + fees.ToString() +
                "\nMiner's Address: " + minerAddress+
                "\nTransactions: " + String.Join("\n", transactionList);
        }


    }
    
   
}
