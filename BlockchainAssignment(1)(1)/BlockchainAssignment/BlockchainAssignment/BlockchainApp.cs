﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

public enum MiningPreference
{
    Greedy,
    Altruistic,
    Random,
    AddressPreference
}

namespace BlockchainAssignment
{
    // this is the class that actually launches our GUI for us and where it is controlled ie the buttons 

    public partial class BlockchainApp : Form
    {
        Blockchain blockchain;
        private MiningPreference selectedMiningPreference;
        public BlockchainApp()
        {
            InitializeComponent();
            blockchain = new Blockchain();// initialisation
            richTextBox1.Text = " New Blockchain Initialised";
            comboBox1.DataSource = Enum.GetValues(typeof(MiningPreference));


        }

        private void Form1_Load(object sender, EventArgs e)
        {
           
        }

        private void button1_Click(object sender, EventArgs e)
        {

            int index = 0;
            if(Int32.TryParse(textBox1.Text, out index))
            {
                richTextBox1.Text = blockchain.GetBlockAsString(index); // printing our geneusis block to the screen 
                // when the button is clicked
            }

        
        
        }


        






            private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e) // this links to the validate key
            // essentially this method will compare the private and public key for the wallet and ensure they are compatibel

        {

            if(Wallet.Wallet.ValidatePrivateKey(textBox3.Text, textBox2.Text))
            {
                richTextBox1.Text = "keys are valid";
            }
            else
            {
                richTextBox1.Text = "keys are invalid";

            }

        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            String privkey;
            Wallet.Wallet myNewWallet = new Wallet.Wallet(out privkey);
            textBox2.Text = myNewWallet.publicID;
            textBox3.Text = privkey;

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void Fee_TextChanged(object sender, EventArgs e)
        {

        }

        private void Amount_TextChanged(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            Transaction newTransaction = new Transaction(textBox2.Text, ReceiverKey.Text, float.Parse(Amount.Text), float.Parse(Fee.Text), textBox3.Text);
            blockchain.transactionPool.Add(newTransaction);
            richTextBox1.Text = newTransaction.ToString(); //calls to create a new transaction 
            // and adds all the parameters to the screen
        }

        private void button5_Click(object sender, EventArgs e)
        {
            List<Transaction> transactions = blockchain.GetPendingTransactions();
            var watch = new Stopwatch();

            watch.Start();
            Block newBlock = new Block(blockchain.GetLastBlock(), transactions, 0, textBox2.Text);
            watch.Stop();
            blockchain.Blocks.Add(newBlock);
            long ms = watch.ElapsedMilliseconds;
            Console.WriteLine($"threadcount = 1,  Difficulty: {newBlock.difficulty}, Mining completed in {ms} ms\n");

            UpdateText(newBlock.ToString());



        }


        


        private void validateChain_Click(object sender, EventArgs e)
        {
            //contiguity checks
            bool valid = true;

            if (blockchain.Blocks.Count == 1) // for the blockchain to be valid, there must be atleast 1 block inside
            {
                if (!blockchain.validateMerkleRoot(blockchain.Blocks[0]))
                { // if merkle root is not successful then display error message 
                    richTextBox1.Text = "Blockchain is invalid";

                }
                else
                {
                    richTextBox1.Text = "Blockchain is valid";
                }
                
                return;
            }
            // this loop checks hash to prev hash for all blocks and checks transactions via merkle root 
            for(int i=1; i<blockchain.Blocks.Count - 1; i++) // not including geneisis block, 
            {// we create a for loop for all of our blocks in our block chain
                // compare neighboring blocks to ensure they are valid
                if (blockchain.Blocks[i].prevHash != blockchain.Blocks[i - 1].hash || !Blockchain.ValidateHash(blockchain.Blocks[i]) || !blockchain.validateMerkleRoot(blockchain.Blocks[0]))
                { // we check two things 
                   // ensure the prehash of this block matches the hash of the previous block
                   // or we check the merkle root of this block matches previous merkle root block
                   // if this is not the case then we say block is inavlid as we used !
                   // so we basically saying does prev hash either match block before hash or merkleroot
                    richTextBox1.Text = "Blockchain is invalid"; // print the string
                    return;
                                                                 
                }

            }

            if (valid)
            {
                richTextBox1.Text = "Blockchain is valid";
            }
            else
            {
                richTextBox1.Text = "Blockchain is invalid";

            }


        }

        private void checkBalance_Click(object sender, EventArgs e) // checking balance of a nodes wallet
        { // the aim is to when sending money for example in a blockchaain what we are checking here is,
            // does the wallet actually have the amount of money in there that it is wanting to spend or send ie amount 


            richTextBox1.Text = blockchain.GetBalance(textBox2.Text).ToString() + " Assigment coin";
           

        }



        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void UpdateText(String text)
        {
            richTextBox1.Text = text;
        }

        private void button6_Click(object sender, EventArgs e)
        {
            Task.Run(() =>
            {
                while (true)
                {
                    // Create a new block inside of the blockchain passing last block and transactions.
                    Block newBlock = new Block(blockchain.GetLastBlock(), blockchain.GetPendingTransactions(), 0, textBox2.Text);

                    // Perform adaptive difficulty Proof-of-Work
                    // the mineblockwithadaptdifficulty calls the Mine method from block
                    blockchain.AdaptiveDifficultyMine(newBlock, blockchain.Blocks);
                    blockchain.Blocks.Add(newBlock); // after successful mining we add it to the blockchain

                    // Print 
                    richTextBox1.AppendText(newBlock.ToString() + "\n"); // Append instead of replacing text

                    // Add a delay before mining the next block to prevent excessive CPU usage
                    //Thread.Sleep(1000); // Sleep for 1 second before mining the next block
                }
            });
        }





        private void readpendingtransactions_Click(object sender, EventArgs e)
        {
            Console.WriteLine($"Transaction Pool Count: {blockchain.transactionPool.Count}");
            UpdateText(String.Join("\n", blockchain.transactionPool));

        }

        private void button6_Click_1(object sender, EventArgs e)
        {
            var watch = new Stopwatch();
            List<Transaction> transactions = blockchain.GetPendingTransactions();


            watch.Start();
            Block newBlock = new Block(blockchain.GetLastBlock(), transactions, 1, textBox2.Text);
            watch.Stop();
            blockchain.Blocks.Add(newBlock);
            long ms = watch.ElapsedMilliseconds;
            Console.WriteLine($"threadcount = 2,  Difficulty: {newBlock.difficulty}, Mining completed in {ms} ms\n");

            UpdateText(newBlock.ToString());
           




        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }

        

        private void button6_Click_2(object sender, EventArgs e)
        {
            var transactions_sorted = new List<Transaction>();
            List<Transaction> transactions = blockchain.transactionPool;
            switch (selectedMiningPreference)
            {
                case MiningPreference.Greedy:
                    transactions_sorted = transactions.OrderByDescending(t => t.fee).ToList(); // order 

                    // check to see if there are transactions that exist in our transaction pool
                    if (transactions_sorted.Count > 0)
                    {
                        // Select the transaction from the list with the highest fee.
                        Transaction highest_fee_Transaction = transactions_sorted[0];

                        // Create a list to have this transaction inside 
                        List<Transaction> greedy_TransactionList = new List<Transaction> { highest_fee_Transaction };

                        // do Mining using greedy and use this trnasaction from the list created ie the highest fee transaction
                        Block newBlock = new Block(blockchain.GetLastBlock(), greedy_TransactionList, 0, textBox2.Text);
                        blockchain.Blocks.Add(newBlock);

                        blockchain.transactionPool.Remove(highest_fee_Transaction);

                        UpdateText(newBlock.ToString());
                    }
                             
                    break;
                case MiningPreference.Altruistic: // case to define the criteria for Altruistic Mining 
        
                    // Ensure there are transactions inside of our transaction pool
                    if (transactions.Count > 0)
                    {
                        // Sort transactions by oldest to newest using the timestamp of when it was created.
                        transactions_sorted = transactions.OrderBy(t => t.timestamp).ToList();

                        // Get the oldest transaction from this list 
                        Transaction oldest_transaction_inside = transactions_sorted[0];

                        // Create a list with only the oldest transaction inside of it for us to use 
                        List<Transaction> singleTransactionList = new List<Transaction> { oldest_transaction_inside };

                        // Mine a new block with the oldest transaction using the regular Mining process defined previously for single thread mining 
                        Block newBlock = new Block(blockchain.GetLastBlock(), singleTransactionList, 0, textBox2.Text);
                        blockchain.Blocks.Add(newBlock);

                        // Remove mined transaction from the pool
                        blockchain.transactionPool.Remove(oldest_transaction_inside);

                        // Update UI with this info 
                        UpdateText(newBlock.ToString());
                    }
                    
                    break;
                case MiningPreference.Random: // case defined for random
                    
                    // Ensure there are transactions inside of the transaction pool
                    if (transactions.Count > 0)
                    {
                        // Randomly select one 
                        Random random = new Random();
                        int randomIndex = random.Next(0, transactions.Count);
                        Transaction random_transaction = transactions[randomIndex];

                        // Create a list with only the random transaction inside of it
                        List<Transaction> random_TransactionList = new List<Transaction> { random_transaction };

                        // Mine a new block with the random transaction method to choose any transaction
                        Block newBlock = new Block(blockchain.GetLastBlock(), random_TransactionList, 0, textBox2.Text);
                        blockchain.Blocks.Add(newBlock);

                        // Remove mined transaction from the pool
                        blockchain.transactionPool.Remove(random_transaction);

                        // Update UI to show this new functionality.
                        UpdateText(newBlock.ToString());
                    }
                    
                    break;
                case MiningPreference.AddressPreference: // case defined for address preference method
                    Console.WriteLine("AddressPreference");

                    // Ensure there are transactions inside of our pool
                    if (transactions.Count > 0)
                    {
                        // capture transactions involving the miner's address based on the sender or receiver address
                        List<Transaction> addressbased_Transactions = transactions.Where(t =>
                            t.senderAddress == address.Text || t.recipientAddress == address.Text
                        ).ToList();

                        if (addressbased_Transactions.Count > 0)
                        {
                            // Select the first preferred transaction (you can modify the selection logic if needed)
                            Transaction selectedTransaction = addressbased_Transactions[0];

                            // Create a list with the selected transaction
                            List<Transaction> singleTransactionList = new List<Transaction> { selectedTransaction };

                            // Mine a new block with the selected transaction
                            Block newBlock = new Block(blockchain.GetLastBlock(), singleTransactionList,0, textBox2.Text);
                            blockchain.Blocks.Add(newBlock);

                            // Remove the mined transaction from the pool
                            blockchain.transactionPool.Remove(selectedTransaction);


                            // Update UI
                            UpdateText(newBlock.ToString());
                        }
                        else
                        {
                            Console.WriteLine("Transaction with this address does not exist.");
                        }
                    }
                    else
                    {
                        Console.WriteLine("No transactions available to mine.");
                    }
                    break;
                default:
                    break;
            }

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            selectedMiningPreference = (MiningPreference)comboBox1.SelectedItem;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void address_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
