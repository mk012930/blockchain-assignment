﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace BlockchainAssignment
{
     class Transaction

    {
        /* Transaction Variables */
        public DateTime timestamp; // Time of creation
        public String senderAddress, recipientAddress; // Participants public key addresses
        public double amount;
        public double fee; // amount is how much money you want to send in your transaction
        // fee is a cost asscoiated with sending a transaction
        public String hash;
        String signature; 

        /* Transaction Constructor */
        public Transaction(String from, String to, double amount, double fee, String privateKey)
        {
            timestamp = DateTime.Now;
            senderAddress = from; // it is going from sender address 
            recipientAddress = to; // to reciepients address based on public keys
            this.amount = amount; // these are just intialising variables in constructor
            this.fee = fee;
            this.hash = CreateHash(); // Hash the transaction attributes
            this.signature = Wallet.Wallet.CreateSignature(from, privateKey, hash); // Sign the hash with the senders private key ensuring validity
        }

        /* Hash the transaction attributes using SHA256 */
        // create mash function is used to create the merkle root 
        // this is a hash of all the transactions and it uses SHA256
        public String CreateHash() // same way block.cs creates the hash
        {
            String hash = String.Empty;
            SHA256 hasher = SHA256Managed.Create();

            /* add all transaction attributes together */
            String input = timestamp + senderAddress + recipientAddress + amount + fee;

            /* Apply the hash function to the "input" string */
            Byte[] hashByte = hasher.ComputeHash(Encoding.UTF8.GetBytes(input));

            /* Reformat to a string */
            foreach (byte x in hashByte)
                hash += String.Format("{0:x2}", x);

            return hash; // final hash is created
        }

        // Represent a transaction as a string for output to UI
        public override string ToString() // function that returns all values in the transaction
            // it will show it on the actual UI page for us to see showing all these attributes and its values 

        {
            return "  [TRANSACTION START]"
                + "\n  Timestamp: " + timestamp.ToString()
                + "\n  -- Verification --"
                + "\n  Transaction Hash: " + hash
                + "\n  Digital Signature: " + signature // the wallet identity 
                + "\n  -- Quantities and amounts --"
                + "\n  Transferred: " + amount + " Assignment Coin"
                + "\t  Fees: " + fee
                + "\n  -- Participants inside transaction --"
                + "\n  Sender Address: " + senderAddress
                + "\n  Recievers Address: " + recipientAddress
                + "\n  [TRANSACTION END]";
        }
    }

}
